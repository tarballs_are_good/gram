/* gram: the miniature bignum library
 *
 * Copyright (c) 2010-2015 Robert Smith
 *
 * This software falls under a BSD 3-clause license. Please see
 * LICENSE.txt for details on use and distribution.
 */
#ifndef GRAM_H
#define GRAM_H

#include <stddef.h> /* size_t */

struct mpz_t;
typedef struct mpz_t *mpz_t;

mpz_t make_mpz(size_t len);
void release_mpz(mpz_t x);
mpz_t copy_mpz(mpz_t original, size_t additional_storage);
int mpz_is_zero(mpz_t x);
int mpz_is_positive(mpz_t x);
int mpz_is_negative(mpz_t x);

mpz_t mpz_add(mpz_t u, mpz_t v);

#endif /* GRAM_H */
