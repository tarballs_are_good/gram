/* gram: the miniature bignum library
 *
 * Copyright (c) 2010-2015 Robert Smith
 *
 * This software falls under a BSD 3-clause license. Please see
 * LICENSE.txt for details on use and distribution.
 */

/* To-do list:
   [ ] = not implemented
   [~] = can be done with other functions
   [x] = implemented

   UTILITY & CONVERSION FUNCTIONS
   ------------------------------
   [~] Alloc       [x] Clear
   [x] Copy

   [ ] To Float    [ ] To String
   [x] From String

   BASIC ARITMETIC OPERATIONS
   --------------------------
   [x] Add         [x] Machine Add
   [x] Sub         [x] Machine Sub
   [~] Mul         [x] Machine Mul
   [x] Div         [x] Machine Div
   [x] Sqrt        [~] Power

   PRIMITIVE & UTILITY ARITHMETIC OPERATIONS
   -----------------------------------------
   [x] Negate      [x] Reciprocal
   [x] Full Mul    [x] Half Mul
   [~] Square

   COMPARISON FUNCTIONS
   --------------------
   [ ] Greater     [ ] Less
   [~] Equal

   BITWISE OPERATIONS
   ------------------
   [ ] And         [ ] Or
   [ ] Not         [ ] Xor
   [x] LSH         [x] RSH
 */

#include "gram.h"

#include <math.h>
#include <stdio.h>  /* printf, perror */
#include <stdlib.h>
#include <stddef.h> /* size_t */
#include <string.h> /* strlen, memcpy, memset */
#include <assert.h>  /* assert */
#include <stdint.h> /* uint16_t */


typedef uint16_t limb_t;

struct mpz_t
{
  size_t len;
  int8_t sign; /* +1 or -1 */
  limb_t* limbs;
};


#define NOP 0
#define LSH 2
#define STO 4
#define RCL 8

/* these values must be less than half the maximum absolute
   representable limb */
#define BASE_INTEGER 10000
#define BASE_FLOAT 10000.0
#define MF 16 /* 2^(log_10(BASE_INTEGER)) */
#define RECIP_BASE_FLOAT (1.0/BASE_FLOAT)

#define PI 3.141592653589793
#define TWOPI 6.28318530717959

#define ALLOC_VECTOR(__n, __t, __v)                                     \
    (__v) = malloc((__n) * sizeof (__t));                               \
    if (NULL == (__v)) perror("memory error allocating vector");        \

void init_mpz(struct mpz_t* b, size_t len)
{
    b->len = len;
    b->sign = 1;
    b->limbs = calloc(len, sizeof (limb_t));
    if (NULL == b->limbs)
        perror("memory error allocating mpz");
}

mpz_t make_mpz(size_t len)
{
  mpz_t r = malloc(sizeof (struct mpz_t));
  if (NULL == r) return (mpz_t)NULL;

  init_mpz(r, (0 == len) ? 1 : len);

  return r;
}

void release_mpz(mpz_t x)
{
  if (NULL == x) return;

  /* free the limbs if necessary */
  if (NULL != x->limbs) free(x->limbs);

  /* we know x is not null, so free it too */
  free(x);
}

mpz_t copy_mpz(mpz_t original, size_t additional_storage)
{
  if (NULL == original) {
    return NULL;
  } else {
    size_t new_len = original->len + additional_storage;
    mpz_t copy = make_mpz(new_len);

    copy->len = new_len;
    copy->sign = original->sign;
    memcpy(copy->limbs, original->limbs, original->len);

    return copy;
  }
}

int mpz_is_zero(mpz_t x) {
  int i;
  for (i = 0; i < x->len; i++)
    if (0 != x->limbs[i])
      return 0;

  return 1;
}

int mpz_is_positive(mpz_t x) {
  if (mpz_is_zero(x)) {
    return 0;
  } else {
    return (x->sign > 0);
  }
}

int mpz_is_negative(mpz_t x) {
  if (mpz_is_zero(x)) {
    return 0;
  } else {
    return (x->sign < 0);
  }
}

/* w := u + v
 *
 * w is assumed to have 1 + max(u_len, v_len) storage at least
 */
void gram_add(limb_t* w, limb_t* u, size_t u_len, limb_t* v, size_t v_len)
{
  if (u_len < v_len) {
    gram_add(w, v, v_len, u, u_len);
  } else {
    /* we know the size of u is at least that of v */
    /* do the first part of the sum */
    int i;
    limb_t s, c = 0;
    for (i = 0; i < v_len; i++) {
      s = c + u[i] + v[i];
      if (s >= BASE_INTEGER) {
        w[i] = s - BASE_INTEGER;
        c = 1;
      } else {
        w[i] = s;
        c = 0;
      }
    }

    /* we may still have portions of u left to add */
    for (i = v_len; i < u_len; i++) {
      s = c + u[i];
      if (s >= BASE_INTEGER) {
        w[i] = s - BASE_INTEGER;
        c = 1;
      } else {
        w[i] = s;
        c = 0;
      }
    }

    /* lastly, we may have a carry */
    w[u_len] = c;
  }
}

mpz_t mpz_add(mpz_t u, mpz_t v)
{
  mpz_t sum = make_mpz(u->len + v->len + 1);

  gram_add(sum->limbs, u->limbs, u->len, v->limbs, v->len);

  return sum;
}

/* subtraction : bignum - bignum */
/* w := u - v                    */
limb_t gram_sub(limb_t* w, limb_t* u, limb_t* v, int n)
{
    int j, s, c = 0;
    for(j = n - 1; j >= 0; j--)
    {
        s = u[j] - v[j] - c;
        if(s < 0)
        {
            w[j] = (limb_t)(s + BASE_INTEGER);
            c = 1;
        }
        else
        {
            w[j] = (limb_t)s;
            c = 0;
        }
    }
    return c;
}

limb_t mpz_sub(struct mpz_t w, struct mpz_t u, struct mpz_t v)
{
    int j, s, c = 0;
    int n;

    n = w.len; /* u.len? v.len? */

    /* assert size? */

    for(j = n - 1; j >= 0; j--)
    {
        s = u.limbs[j] - v.limbs[j] - c;
        if(s < 0)
        {
            w.limbs[j] = (limb_t)(s + BASE_INTEGER);
            c = 1;
        }
        else
        {
            w.limbs[j] = (limb_t)s;
            c = 0;
        }
    }
    return c;
}

/* addition : bignum + machine */
/* w := u + iv                 */
limb_t gram_machine_add(limb_t* w, limb_t* u, int n, int iv)
{
    int j, s, c = iv;
    for(j = n - 1; j >= 0; j--)
    {
        s = u[j] + c;
        if(s >= BASE_INTEGER)
        {
            w[j] = (limb_t)(s - BASE_INTEGER);
            c = 1;
        }
        else
        {
            w[j] = (limb_t)s;
            c = 0;
            break;
        }
    }
    return c;
}

limb_t mpz_machine_add(struct mpz_t w, struct mpz_t u, int iv)
{
    int j, s, c = iv;

    assert(w.len >= u.len);

    for(j = u.len - 1; j >= 0; j--)
    {
        s = u.limbs[j] + c;
        if(s >= BASE_INTEGER)
        {
            w.limbs[j] = (limb_t)(s - BASE_INTEGER);
            c = 1;
        }
        else
        {
            w.limbs[j] = (limb_t)s;
            c = 0;
            break;
        }
    }
    return c;
}

/* subtraction : bignum - machine */
/* w := u - iv                    */
limb_t gram_machine_sub(limb_t* w, limb_t* u, int n, int iv)
{
    int j, s, c = iv;
    for(j = n - 1; j >= 0; j--)
    {
        s = u[j] - c;
        if(s < 0)
        {
            w[j] = (limb_t)(s + BASE_INTEGER);
            c = 1;
        }
        else
        {
            w[j] = (limb_t)s;
            c = 0;
            break;
        }
    }
    return c;
}

limb_t mpz_machine_sub(struct mpz_t w, struct mpz_t u, int iv)
{
    int j, s, c = iv;

    assert(w.len >= u.len);

    for(j = u.len - 1; j >= 0; j--)
    {
        s = u.limbs[j] - c;
        if(s < 0)
        {
            w.limbs[j] = (limb_t)(s + BASE_INTEGER);
            c = 1;
        }
        else
        {
            w.limbs[j] = (limb_t)s;
            c = 0;
            break;
        }
    }
    return c;
}

/* multiplication : bignum * machine */
/* w := u * iv                       */
limb_t gram_machine_mul(limb_t* w, limb_t* u, int n, int iv)
{
    int j, s, c = 0;
    for(j = n - 1; j >= 0; j--)
    {
        s = u[j] * iv + c;
        w[j] = (limb_t)(s % BASE_INTEGER);
        c = s / BASE_INTEGER;
    }
    return c;
}

limb_t mpz_machine_mul(struct mpz_t w, struct mpz_t u, int iv)
{
    int j, s, c = 0;

    assert(w.len >= u.len);

    for(j = u.len - 1; j >= 0; j--)
    {
        s = u.limbs[j] * iv + c;
        w.limbs[j] = (limb_t)(s % BASE_INTEGER);
        c = s / BASE_INTEGER;
    }
    return c;
}

/* division : bignum / machine */
/* w := u / iv                 */
limb_t gram_machine_div(limb_t* w, limb_t* u, int n, int iv)
{
    int j, s, r = 0;
    for(j = 0; j < n; j++)
    {
        s = BASE_INTEGER * r + u[j];
        w[j] = (limb_t)(s / iv);
        r = s % iv;
    }
    return r;
}

limb_t mpz_machine_div(struct mpz_t w, struct mpz_t u, int iv)
{
    int j, s, r = 0;

    assert(w.len >= u.len);

    for(j = 0; j < u.len; j++)
    {
        s = BASE_INTEGER * r + u.limbs[j];
        w.limbs[j] = (limb_t)(s / iv);
        r = s % iv;
    }
    return r;
}

/* negate : -bignum */
/* u := -u          */
limb_t gram_neg(limb_t* u, int n)
{
    int j, s, c = 0;
    for(j = n - 1; j >= 0; j--)
    {
        s = -u[j] - c;
        if(s < 0)
        {
            u[j] = (limb_t)(s + BASE_INTEGER);
            c = 1;
        }
        else
        {
            u[j] = (limb_t)s;
            c = 0;
        }
    }
    return c;
}

limb_t mpz_neg(struct mpz_t x)
{
    int j, s, c = 0;

    for(j = x.len - 1; j >= 0; j--)
    {
        s = -x.limbs[j] - c;
        if(s < 0)
        {
            x.limbs[j] = (limb_t)(s + BASE_INTEGER);
            c = 1;
        }
        else
        {
            x.limbs[j] = (limb_t)s;
            c = 0;
        }
    }

    return c;
}

/* copy : bignum -> bignum */
void gram_cpy(limb_t* u, limb_t* v, int n)
{
    int j;
    for(j = 0; j < n; j++)
    {
        u[j] = v[j];
    }
}

void mpz_cpy(struct mpz_t u, struct mpz_t v)
{
    int j;

    assert(u.len >= v.len);

    /* FIXME: memcpy this */
    for(j = 0; j < v.len; j++)
    {
        u.limbs[j] = v.limbs[j];
    }
}

/* left shift : bignum * BASE_INTEGER */
void gram_lsh(limb_t* u, int n)
{
    int j;
    for(j = 0; j < n - 1; j++)
    {
        u[j] = u[j + 1];
    }
    u[n - 1] = 0;
}

void mpz_lsh(struct mpz_t x)
{
    int j;
    for(j = 0; j < x.len - 1; j++)
    {
        x.limbs[j] = x.limbs[j + 1];
    }
    x.limbs[x.len - 1] = 0;
}

/* clear : bignum = 0 */
void gram_clr(limb_t* u, int n)
{
    int j;
    for(j = 0; j < n; j++)
    {
        u[j] = 0;
    }
}

void mpz_clr(struct mpz_t x)
{
    int j;
    for(j = 0; j < x.len; j++)
    {
        x.limbs[j] = 0;
    }
}

void make_same_size(struct mpz_t* u, struct mpz_t* v)
{
    struct mpz_t vv;

    if (u->len < v->len)
    {
        make_same_size(v, u);
    }
    else if (u->len > v->len)
    {
        init_mpz(&vv, u->len);

        memcpy(vv.limbs, v->limbs, v->len*sizeof(limb_t));

        free(v->limbs);

        v->len   = vv.len;
        v->limbs = vv.limbs;
    }

    return;
}

void make_mul_size(struct mpz_t* w, struct mpz_t* u)
{
    struct mpz_t vv;

    if (w->len < 2*u->len)
    {
        init_mpz(&vv, 2*u->len);
        memcpy(vv.limbs, w->limbs, w->len*sizeof(limb_t));

        free(w->limbs);

        w->len   = vv.len;
        w->limbs = vv.limbs;
    }

    return;
}

/*******************************************************************/

/* 1D complex FFT */
void complex_fft(double* data, unsigned long nn, int isign)
{
    unsigned long n, mmax, m, j, istep, i;
    double omegatmp, omegar, omegapr, omegapi, omegai, theta;
    double tmpr, tmpi;
    n = nn << 1;
    j = 1;
    for(i = 1; i < n; i += 2)
    {
        if(j > i)
        {
            tmpr = data[j - 1];
            data[j - 1] = data[i - 1];
            data[i - 1] = tmpr;

            tmpr = data[j];
            data[j] = data[i];
            data[i] = tmpr;
        }
        m = n >> 1;
        while(m >= 2 && j > m)
        {
            j -= m;
            m >>= 1;
        }
        j += m;
    }
    mmax = 2;
    while(n > mmax)
    {
        istep = mmax << 1;
        theta = isign*(TWOPI/mmax);
        omegatmp = sin(0.5 * theta);
        omegapr = -2.0*omegatmp*omegatmp;
        omegapi = sin(theta);
        omegar = 1.0;
        omegai = 0.0;
        for(m = 1; m < mmax; m += 2)
        {
            for(i = m; i <= n; i += istep)
            {
                j = i + mmax;
                tmpr = omegar*data[j - 1] - omegai*data[j];
                tmpi = omegar*data[j]     + omegai*data[j - 1];
                data[j - 1] = data[i - 1] - tmpr;
                data[j]     = data[i]     - tmpi;
                data[i - 1] += tmpr;
                data[i]     += tmpi;
            }
            omegatmp = omegar;
            omegar = omegar*omegapr - omegai*omegapi + omegar;
            omegai = omegai*omegapr + omegatmp*omegapi + omegai;
        }
        mmax = istep;
    }
}

/* 1D real FFT */
void real_fft(double* data, unsigned long n, int isign)
{
    unsigned long i, i1, i2, i3, i4, np1;
    double c1 = 0.5, c2, h1r, h1i, h2r, h2i;
    double omegar, omegai, omegapr, omegapi, omegatmp, theta;

    theta = PI/(double)(n >> 1);
    if(1 == isign)
    {
        c2 = -0.5;
        complex_fft(data, n >> 1, 1);
    }
    else
    {
        c2 = 0.5;
        theta = -theta;
    }
    omegatmp = sin(0.5*theta);
    omegapr = -2.0*omegatmp*omegatmp;
    omegapi = sin(theta);
    omegar = 1.0 + omegapr;
    omegai = omegapi;
    np1 = n + 1;
    for(i = 2; i <= (n >> 2); i++)
    {
        i1 = i + i - 2;
        i2 = 1 + i1;
        i3 = np1 - i2;
        i4 = 1 + i3;
        h1r =  c1*(data[i1] + data[i3]);
        h1i =  c1*(data[i2] - data[i4]);
        h2r = -c2*(data[i2] + data[i4]);
        h2i =  c2*(data[i1] - data[i3]);
        data[i1] = h1r + omegar * h2r - omegai * h2i;
        data[i2] = h1i + omegar * h2i + omegai * h2r;
        data[i3] = h1r - omegar * h2r + omegai * h2i;
        data[i4] = -h1i + omegar * h2i + omegai * h2r;
        omegatmp = omegar;
        omegar = omegar*omegapr - omegai*omegapi + omegar;
        omegai = omegai*omegapr + omegatmp*omegapi + omegai;
    }
    if(1 == isign)
    {
        h1r = data[0];
        data[0] = h1r + data[1];
        data[1] = h1r - data[1];
    }
    else
    {
        h1r = data[0];
        data[0] = c1*(h1r + data[1]);
        data[1] = c1*(h1r - data[1]);
        complex_fft(data, n >> 1, -1);
    }
}



/* full multiplication : bignum * bignum */
/* w := u*v                              */
void gram_full_mul(limb_t* w, limb_t* u, limb_t* v, int n, int m, int flags)
{
    static double* a, * b, * c;
    long j, nn;
    double cy, t;

    nn = n << 1;
    ALLOC_VECTOR(nn, double, a);
    ALLOC_VECTOR(nn, double, b);
    ALLOC_VECTOR(nn >> 1, double, c);

    if((flags & RCL) && (u == v)) /* a := c */
    {
        for(j = 0; j < nn; j++)
            a[j] = c[j];
    }
    else /* a := u ++ {0,...,0} */
    {
        for(j = 0; j < n; j++)
            a[j] = (double)u[j];
        for(j = n; j < nn; j++)
            a[j] = 0.0;
        real_fft(a, nn, 1);
    }

    if(flags & RCL) /* b := c */
    {
        for(j = 0; j < nn; j++)
            b[j] = c[j];
    }
    else if(u == v) /* b := a */
    {
        for(j = 0; j < nn; j++)
            b[j] = a[j];
    }
    else /* b := v ++ {0,...,0} */
    {
        for(j = 0; j < n; j++)
            b[j] = (double)v[j];
        for(j = n; j < nn; j++)
            b[j] = 0.0;
        real_fft(b, nn, 1);
    }

    if(flags & STO) /* c := b */
    {
        for(j = 0; j < nn; j++)
            c[j] = b[j];
    }

    b[0] *= a[0];
    b[1] *= a[1];

    for(j = 2; j < nn; j += 2)
    {
        t = b[j];
        b[j]     = t*a[j]     - b[j + 1]*a[j + 1];
        b[j + 1] = t*a[j + 1] + b[j + 1]*a[j];
    }

    real_fft(b, nn, -1);
    cy = 0.0;

    for(j = nn - 1; j >= 0; j--)
    {
        t = b[j] / (nn >> 1) + cy + 0.5;
        cy = (unsigned long)(t / BASE_FLOAT);
        b[j] = t - cy * BASE_FLOAT;
    }

    if(cy >= BASE_FLOAT)
        perror("impossible case");

    if(flags & LSH) /* w := b */
    {
        for(j = 0; j < m; j++)
            w[j] = (limb_t)b[j];
    }
    else /* w := {cy} ++ b */
    {
        w[0] = (limb_t)cy;
        for(j = 1; j < m; j++)
            w[j] = (limb_t)b[j - 1];
    }

    free(c);
    free(b);
    free(a);
}

void mpz_full_mul(struct mpz_t w, struct mpz_t u, struct mpz_t v, int flags)
{
    static double* a, * b, * c;
    long j, nn;
    int n, m;
    double cy, t;

    n = u.len;
    m = w.len;

    nn = n << 1;
    ALLOC_VECTOR(nn, double, a);
    ALLOC_VECTOR(nn, double, b);
    ALLOC_VECTOR(nn >> 1, double, c);

    if((flags & RCL) && (&u == &v)) /* a := c */
    {
        memcpy(a, c, nn*sizeof(double));
    }
    else /* a := u ++ {0,...,0} */
    {
        for(j = 0; j < n; j++)
            a[j] = (double)u.limbs[j];

        /* TODO: change to memset */
        for(j = n; j < nn; j++)
            a[j] = 0.0;

        real_fft(a, nn, 1);
    }

    if(flags & RCL) /* b := c */
    {
        memcpy(b, c, nn*sizeof(double));
    }
    else if(&u == &v) /* b := a */
    {
        memcpy(b, a, nn*sizeof(double));
    }
    else /* b := v ++ {0,...,0} */
    {
        for(j = 0; j < n; j++)
            b[j] = (double)v.limbs[j];

        /* TODO: change to memset */
        for(j = n; j < nn; j++)
            b[j] = 0.0;

        real_fft(b, nn, 1);
    }

    if(flags & STO) /* c := b */
    {
        memcpy(c, b, nn*sizeof(double));
    }

    b[0] *= a[0];
    b[1] *= a[1];

    for(j = 2; j < nn; j += 2)
    {
        t = b[j];
        b[j]     = t*a[j]     - b[j + 1]*a[j + 1];
        b[j + 1] = t*a[j + 1] + b[j + 1]*a[j];
    }

    real_fft(b, nn, -1);
    cy = 0.0;

    for(j = nn - 1; j >= 0; j--)
    {
        t = b[j] / (nn >> 1) + cy + 0.5;
        cy = (unsigned long)(t / BASE_FLOAT);
        b[j] = t - cy * BASE_FLOAT;
    }

    if(cy >= BASE_FLOAT)
        perror("impossible case");

    if(flags & LSH) /* w := b */
    {
        for(j = 0; j < m; j++)
            w.limbs[j] = (limb_t)b[j];
    }
    else /* w := {cy} ++ b */
    {
        w.limbs[0] = (limb_t)cy;
        for(j = 1; j < m; j++)
            w.limbs[j] = (limb_t)b[j - 1];
    }

    /* XXX FIXME no idea why this garbage errors */
    /*    free(c);     */ /* FIXME: this will leak!!!!!!!!!!!!!!!!!!! */
    /*free(b);
    free(a);
    */
}

/* half multiply : bignum * bignum, only upper limbs multiplied */
/* w := u * v */
void gram_half_mul(limb_t* w, limb_t* u, limb_t* v, int n, int m, int flags)
{
    int h, i, j, k;
    h = n >> 1;

    for(i = 0; i < h && u[i] == 0; i++) /*nothing*/;
    for(j = 0; j < h && v[j] == 0; j++) /*nothing*/;

    k = i + j - 1;

    if(k < 0)
    {
        gram_full_mul(w, u, v, h, m, flags | LSH);
    }
    else
    {
        gram_full_mul(w + k, u + i, v + j, h, m - k, flags & ~LSH);
        for(i = 0; i < k; i++)
            w[i] = 0;
    }
}

void mpz_half_mul(struct mpz_t w, struct mpz_t u, struct mpz_t v, int flags)
{
    int h, i, j, k;
    int n, m;
    struct mpz_t uu = u, vv = v, ww = w;

    n = u.len;
    m = w.len;
    h = n >> 1;

    for(i = 0; i < h && u.limbs[i] == 0; i++) /*nothing*/;
    for(j = 0; j < h && v.limbs[j] == 0; j++) /*nothing*/;

    k = i + j - 1;

    if(k < 0)
    {
        uu.len = h;
        vv.len = h;
        ww.len = m;
        mpz_full_mul(ww, u, v, flags | LSH);
    }
    else
    {
        uu.len = h;
        vv.len = h;
        ww.len = m - k;

        ww.limbs += k; /* shift pointers */
        uu.limbs += i;
        vv.limbs += j;

        mpz_full_mul(ww, uu, vv, flags & ~LSH);

        /* TODO: memset */
        for(i = 0; i < k; i++)
            w.limbs[i] = 0;
    }

    return;
}

/*******************************************************************/

/* square root : sqrt(bignum) */
void gram_sqrt(limb_t* y, limb_t* x, limb_t* a, int nn)
{
    static limb_t* u;
    int i, j, mm, isign, n = nn, k, kk;
    double fu, fv;

    ALLOC_VECTOR(n, limb_t, u);

    mm = (n < MF) ? n : MF;
    fv = (double)a[mm];

    for(j = mm - 1; j >= 0; j--)
    {
        fv *= RECIP_BASE_FLOAT;
        fv += a[j];
    }

    fu = 1.0/sqrt(fv);

    for(j = 0; j < n; j++)
    {
        i = (int)fu;
        x[j] = (limb_t)i;
        fu = BASE_FLOAT * (fu - i);
    }

    for(n = 8; n <= nn; n <<= 2)
    {
        kk = (n == nn>>4)?2:1;

        for(k = 0; k < kk; k++)
        {
            /* Recurrence: x := x + x*(1 - A*x^2)/2 */
            gram_full_mul(u, x, x, n, n, LSH);   /* u := x^2 */
            gram_full_mul(u, a, u, n, n, LSH);   /* u := a*u */
            if(0 == u[0])                        /* u := 1 - u */
            {
                gram_neg(u, n);
                u[0] -= 9999; /* XXX: base dependent */
                isign = 1;
            }
            else
            {
                u[0]--;
                isign = -1;
            }

            gram_half_mul(u, u, x, n, n, LSH);   /* u := x*u */
            gram_machine_div(u, u, n, 2);        /* u := u/2 */
            if(1 == isign)                       /* x := x + u */
              gram_add(x, x, n, u, n);
            else
              gram_sub(x, x, u, n);
        }
    }
    free(u);
    return;
}

/* reciprocal : 1/bignum */
void gram_recip(limb_t* x, limb_t* a, int nn)
{
    static limb_t* u;
    int i, j, mm, isign, n = nn, k, kk;
    double fu, fv;

    ALLOC_VECTOR(n, limb_t, u);

    mm = (n < MF)?n:MF;
    fv = (double)a[mm];
    for(j = mm - 1; j >= 0; j--)
    {
        fv *= RECIP_BASE_FLOAT;
        fv += a[j];
    }

    fu = 1.0/fv;
    for(j = 0; j < n; j++)
    {
        i = (int)fu;
        x[j] = (limb_t)i;
        fu = BASE_FLOAT * (fu - i);
    }

    for(n = 8; n <= nn; n <<= 2)
    {
        kk = (n == nn>>4)?2:1;

        for(k = 0; k < kk; k++)
        {
            /* Recurrence: x := x + x*(1 - A*x) */
            gram_full_mul(u, a, x, n, n, LSH);   /* u := a*x */
            if(0 == u[0])                        /* u := 1 - u */
            {
                gram_neg(u, n);
                u[0] -= 9999; /* XXX: base dependent */
                isign = 1;
            }
            else
            {
                u[0]--;
                isign = -1;
            }
            gram_half_mul(u, x, u, n, n, LSH);   /* u := x*u */
            if(1 == isign)                       /* x := x + u */
              gram_add(x, x, n, u, n);
            else
              gram_sub(x, x, u, n);
        }
    }
    free(u);
    return;
}

/*******************************************************************/

limb_t char_to_limb(char c)
{
    return ((limb_t)(c - '0'));
}

struct mpz_t str_to_mpz(const char* s)
{
    /* FIXME: Highly base dependent. */
    static int ten[4] = {1, 10, 100, 1000};

    int slen, i;
    struct mpz_t result;

    slen = strlen(s);

    init_mpz(&result, ceil(slen/4.0));

    for (i = 0; i < slen; i++)
        result.limbs[i/4] += ten[i%4]*char_to_limb(s[slen - i - 1]);

    return result;
}

void show(const char* st, const char* en, limb_t* u, int n)
{
    /* FIXME: Highly base dependent. */
    int i, skipzeros = 1, firstprinted = 0;

    printf("%s",st);
    for (i = n - 1; i >= 0; i--)
    {
        if (0 != u[i] || 0 == skipzeros)
        {
            skipzeros=0;
            if (0 == firstprinted)
            {
                firstprinted = 1;
                printf("%d", u[i]);
            }
            else
            {
                printf("%04d", u[i]);
            }
        }
    }

    if (1 == skipzeros)
        printf("%d", 0);

    printf("%s",en);

    return;
}
