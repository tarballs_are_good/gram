all:
	gcc -ansi -Wall -Werror -fpic -c gram.c
	gcc -shared -o libgram.so gram.o

clean:
	rm -f gram.o libgram.so
