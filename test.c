#include "gram.h"

#define SHOWNUM(_v, _l) show("", "\n", (_v), (_l))
#define SHOWMPZ(_b) show("", "\n", (_b).limbs, (_b).len)

/*******************************************************************/

int main(int argc, char** argv) {
    struct mpz_t a, b, c, d, x;

    init_mpz(&x, 6);

    a = str_to_mpz("545140134");
    b = str_to_mpz("13591409");
    c = str_to_mpz("640320");
    d = str_to_mpz("57439768479798135923051290590236739670496408640963");

#define PRINTALL()                                                      \
    printf("a:%4u: ", a.len); SHOWMPZ(a);                               \
    printf("b:%4u: ", b.len); SHOWMPZ(b);                               \
    printf("c:%4u: ", c.len); SHOWMPZ(c);                               \
    printf("d:%4u: ", d.len); SHOWMPZ(d);                               \
    printf("x:%4u: ", x.len); SHOWMPZ(x); printf("\n");

    PRINTALL();

    mpz_add(x, b, c);

    PRINTALL();

    make_same_size(&a, &b);
    make_same_size(&a, &c);
    make_mul_size(&x, &d);

    PRINTALL();

    mpz_clr(x);

    PRINTALL();

    mpz_full_mul(x, d, d, 0);

    PRINTALL();


    /*
    SHOWNUM(x, 3);
    SHOWNUM(y, 3);

    gram_full_mul(z, x, y, 4, 6, 0);

    SHOWNUM(z, 10);

    SHOWNUM(x, 3);

    gram_neg(x, 3);

    SHOWNUM(x, 3);

    gram_add(xx, y, x, 3);

    SHOWNUM(xx, 4);
    */
    free_mpz(a);
    free_mpz(b);
    free_mpz(c);
    free_mpz(d);
    free_mpz(x);

    return 0;
}
